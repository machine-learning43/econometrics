# Econometric Analysis

This was an assignment for an Econometrics class that required to perform an analysis of the 'STAR_Students.tab’ dataset either in R or Python. The code performs statistical analysis in Python and the report provides the interpretation. 
