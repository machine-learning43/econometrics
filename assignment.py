import os
import numpy as np
import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt
import scipy.stats as stats
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from statsmodels.stats.diagnostic import het_white


##############################################################################
# Helper function to transform results summary into a dataFrame
##############################################################################

def results_summary_to_dataframe(results, rounding=2):
    '''take the result of an statsmodel results table
    and transform it into a dataframe.'''

    # get the values from results
    # if you want, you can of course generalize this.
    # e.g. if you don't have normal error terms
    # you could change the pvalues and confidence bounds
    # see exercise session 9?!
    pvals = results.pvalues
    tvals = results.tvalues
    coeff = results.params
    conf_lower = results.conf_int()[0]
    conf_higher = results.conf_int()[1]

    # create a pandas DataFrame from a dictionary
    results_df = pd.DataFrame({"pvals": np.round(pvals, rounding),
                               "tvals": np.round(tvals, rounding),
                               "coeff": np.round(coeff, rounding),
                               "conf_lower": np.round(conf_lower, rounding),
                               "conf_higher": np.round(conf_higher, rounding)
                               })
    # This is just to show you how to re-order if needed
    # Typically you should put them in the order you like straigh away
    #Reordering...
    results_df = results_df[["coeff", "tvals", "pvals", "conf_lower",
                             "conf_higher"]]

    return results_df

##############################################################################


def data_frame_to_latex_table_file(file_name, df):
    """takes a DataFrame and creates file_name.tex with LaTeX table data. """
    # create and open file
    text_file = open(file_name, "w")
    # data frame to LaTeX
    df_latex = df.to_latex()
    # Consider extensions (see later in class)
    # write latex string to file
    text_file.write(df_latex)
    # close file
    text_file.close()


def summary_to_latex_table_file(file_name, summary):
    # create and open file
    text_file = open(file_name, "w")
    # data frame to LaTeX
    df_latex = summary.as_latex()
    # Consider extensions (see later in class)
    # write latex string to file
    text_file.write(df_latex)
    # close file
    text_file.close()

###############################################################################
###############################################################################
# Start of Script
###############################################################################
###############################################################################


# first birthday
bd_1 = 3009
# second birthday
bd_2 = 3009

group_seed = bd_1 * bd_2

# set the seed
np.random.seed(group_seed)

# specify the number of observations used in the assignment
num_obs = 8000

###############################################################################
# Set the folders for output of graphs and tables
###############################################################################

# for the figures
figure_dir = '../figures/'
if not os.path.exists(figure_dir):
    os.makedirs(figure_dir)
# for the latex document
report_dir = '../report/'
if not os.path.exists(report_dir):
    os.makedirs(report_dir)


file_name = 'STAR_Students.tab'

full_df = pd.read_csv(file_name, sep='\t')

print('\nQuestion 1. Data Loaded\n')

kg_df = full_df.filter(regex=('gk')).copy()

# select the observation numbers to be included in our analysis
# it creates a vector with randomly selected row numbers from 0 to total
# length of the data without replacement.
observations = np.random.choice(len(kg_df), num_obs, replace=False)

# add the general variables to the kindergarten data set
kg_df['gender'] = full_df['gender']
kg_df['race'] = full_df['race']

# select on the observations for our group
data = kg_df.iloc[observations, :].copy()
print(kg_df)
print(full_df[['gender', 'race']])

print('\nQuestion 2\n')

# description = full_df.describe()
# info = full_df.info()

description = data.describe().T
print(description)
data_frame_to_latex_table_file(report_dir + 'description.tex', np.round(description, 2))

print('\nExported descriptive statistics.\n')

print('\nQuestion 3\n')

data['w/a'] = data.loc[:, 'race'].isin([1,4]).astype(int)
print(data['w/a'])

# or we could use pd.get_dummies()

print('\nDummy Variable Created.\n')

print('\nQuestion 4\n')

# set the dependent variable
y = data['gktmathss']
# select the subset of included explanatory variables
x = data[['w/a', 'gender']]
# add a constant term
X = sm.add_constant(x)

# set up the model and DROP any missing values
model = sm.OLS(y, X, missing='drop')
# run the regression
results = model.fit()

# print a summary
print(results.summary())

summary_to_latex_table_file(report_dir + 'maths_gender_etnicity.tex', 
                            results.summary())

print('\nRegresion Completed. Summary Exported.\n')

print('\nQuestion 5\n')

xnewvars = data[['w/a', 'gender', 'gkfreelunch', 'gkclasssize', 
                'gkclasstype', 'gktyears', 'gkpresent', 'gkabsent']]
newvars = sm.add_constant(xnewvars)
modelnewvars = sm.OLS(y, newvars, missing='drop')
resultnewvars = modelnewvars.fit()
print(resultnewvars.summary())

summary_to_latex_table_file(report_dir + 'newvars.tex', 
                            resultnewvars.summary())

print('\nNew Model Created\n')

print('\nQuestion 6\n')

print('\ngkclasssize and gkpresent are a suspected source of multicollinearity\n')

print('\nTesting gkpresent for possible multicolinearity\n')

multicolin1 = sm.OLS(data['gkpresent'], data[['w/a', 'gender', 
                                             'gkfreelunch', 'gkclasssize', 
                                             'gkclasstype', 'gktyears', 
                                             'gkabsent']], missing='drop')
resultmulticolin1 = multicolin1.fit()
print(resultmulticolin1.params, resultmulticolin1.bse)

multisumm1 = pd.DataFrame(resultmulticolin1.params, columns=['Coefficients'])
multisumm1['Std. Errors'] = resultmulticolin1.bse

print('\nMulticollinearity Found.\n')

data_frame_to_latex_table_file(report_dir + 'multicollin_gkpresent.tex', 
                               multisumm1)
                                                   
print('\nTesting gkclasssize for possible multicolinearity\n')

multicolin2 = sm.OLS(data['gkclasssize'], data[['w/a', 'gender', 
                                             'gkfreelunch', 'gkpresent', 
                                             'gkclasstype', 'gktyears', 
                                             'gkabsent']], missing='drop')

resultmulticolin2 = multicolin2.fit()
print(resultmulticolin2.params, resultmulticolin2.bse)

multisumm2 = pd.DataFrame(resultmulticolin2.params, columns=['Coefficients'])
multisumm2['Std. Errors'] = resultmulticolin2.bse

data_frame_to_latex_table_file(report_dir + 'multicollin_gkclasssize.tex', 
                               multisumm2)
print('\nMulticollinearity Not Found.\n')


print('\nRegression without gkpresent:\n')

xnewvars2 = data[['w/a', 'gender', 'gkfreelunch', 
                 'gkclasstype', 'gktyears', 'gkabsent', 'gkclasssize']]
newvars2 = sm.add_constant(xnewvars2)
modelnewvars2 = sm.OLS(y, newvars2, missing='drop')
resultnewvars2 = modelnewvars2.fit()
print(resultnewvars2.summary()) 

summary_to_latex_table_file(report_dir + 'newvars2.tex', 
                            resultnewvars2.summary())

print('\nSee report for Question 7 and 8.\n')

print('\nQuestion 9.\n')

print("\nHypothesis Test: Is gkclasssize statistically significant to have an impact on the students' performance?\n")

stats_classsize = data['gkclasssize']
stats_classsize = sm.add_constant(stats_classsize)
modelclasssize = sm.OLS(y, stats_classsize, missing='drop')
resultclasssize = modelclasssize.fit()

classsizestat = pd.DataFrame([np.round(resultclasssize.params[1], 3)], 
                             columns=['Coefficient'])
classsizestat['Standard Error'] = np.round(resultclasssize.bse[1], 3)
classsizestat['T-Value'] = resultclasssize.tvalues[1]
classsizestat['P-Value'] = resultclasssize.pvalues[1]

print(classsizestat)

data_frame_to_latex_table_file(report_dir + 'classsize_significance.tex', 
                            classsizestat)

print('\nHypothesis Tested\n')

print('\nQuestion 10.\n')

print("\nLet's test if the overall model makes sense.\n")

ftestmodel = modelnewvars2.fit()
modeltest = pd.Series(np.array([ftestmodel.fvalue, ftestmodel.f_pvalue]), 
                      index = ['F-test', 'P-value']).astype(float)
modeltest.name = 'Model F Test'
print(modeltest)

data_frame_to_latex_table_file(report_dir + 'model_test.tex',
                               modeltest)

print('\nWald Test Completed.\n')

print('\nQuestion 11.\n')

R = np.array([0, 1, 1, 0, 0, 0, 0, 0])
tvalue = R @ resultnewvars2.params / (R @ resultnewvars2.cov_HC0 @ R.T)
pvalue = 2*(1 - stats.norm.cdf(tvalue))
gen_etn_hypo_test = pd.Series(np.array([tvalue, pvalue]), index=['T-value', 
                                                                  'P-value'])
gen_etn_hypo_test.name = 'Hypothesis test for same effect: Gender vs Etnicity'
print('\n', gen_etn_hypo_test,'\n')

data_frame_to_latex_table_file(report_dir + 'gen_etn_hypo_test.tex',
                               gen_etn_hypo_test)


print('\nQuestion 12. See the report.\n')

print('\nQuestion 13.\n')

# We construct confidence and prediction intervals around y and y_bar
# limit the output to the first 5 observations

yconfidence = resultnewvars2.get_prediction().conf_int()

prstd, iv_l, iv_u = wls_prediction_std(resultnewvars2)
yprediction = pd.concat([iv_l, iv_u], axis=1).reset_index()
yprediction.drop('index', inplace=True, axis=1)

meanconf = pd.DataFrame(np.matrix(yconfidence).mean(0))
yconf= pd.DataFrame(yconfidence[0:5])

print('\nConfidence Intervals.\n')
print(meanconf)
print(yconf)

meanpred = pd.DataFrame(np.matrix(yprediction).mean(0))
ypred= pd.DataFrame(yprediction[0:5])

print('\nPrediction Intervals.\n')
print(meanpred)
print(ypred)

# first, we round the intervals and join the lower and upper boundaries into one interval

intervals = np.round(pd.concat([yconf, ypred], axis=1), 2)
intervals.columns=['ConfLower',
                   'ConfUpper',
                   'PredLower',
                   'PredUpper']
intervals = intervals.astype(str)
intervals['Confidence Intervals'] = intervals['ConfLower'] +', '+ intervals['ConfUpper']
intervals['Prediction Intervals'] = intervals['PredLower'] +', '+ intervals['PredUpper']
intervals.drop('ConfLower', inplace=True, axis=1)
intervals.drop('ConfUpper', inplace=True, axis=1)
intervals.drop('PredLower', inplace=True, axis=1)
intervals.drop('PredUpper', inplace=True, axis=1)

# then we do the same for mean intervals

mean_intervals = np.round(pd.concat([meanconf, meanpred], axis=1), 2)
mean_intervals.columns=['ConfLower',
                    'ConfUpper',
                    'PredLower',
                    'PredUpper']
mean_intervals = mean_intervals.astype(str)
mean_intervals['Mean Confidence Interval'] = mean_intervals['ConfLower'] +', '+ mean_intervals['ConfUpper']
mean_intervals['Mean Prediction Interval'] = mean_intervals['PredLower'] +', '+ mean_intervals['PredUpper']
mean_intervals.drop('ConfLower', inplace=True, axis=1)
mean_intervals.drop('ConfUpper', inplace=True, axis=1)
mean_intervals.drop('PredLower', inplace=True, axis=1)
mean_intervals.drop('PredUpper', inplace=True, axis=1)

print('Confidence and Prediction Intervals', intervals)
print('Mean Confidence and Prediction Intervals', mean_intervals)

data_frame_to_latex_table_file(report_dir + 'intervals.tex',
                               intervals)
data_frame_to_latex_table_file(report_dir + 'meanintervals.tex',
                               mean_intervals)

print('\nFormatting finished. Data exported\n')

print('\nQuestion 14.\n')

print("\nLet us perform a White Test for heteroscedasticity and adjust standard errors accordingly.\n")

datadropna = pd.concat([y, newvars2], axis=1).dropna() 
datadropna.drop('gktmathss', inplace=True, axis=1)

white_test = het_white(resultnewvars2.resid, datadropna)

print(white_test)

white = pd.Series(np.array([white_test[2], white_test[3]]), 
                         index=['F-test', 'P-value']).astype(float)
white.name = 'White Test for Heteroscedasticity'
print('\n', white, '\n')

data_frame_to_latex_table_file(report_dir + 'white_test.tex',
                               white)

print('\nHeteroscedasticity Present.\n')
print('\nAdjusting standard errors for heteroscedasticity.\n')

resultnewvars2_adjusted = modelnewvars2.fit(cov_type='HC0')

print('\nTesting statistical significance. Running an F-test on the adjusted model...\n')

print(resultnewvars2_adjusted.summary())
summary_to_latex_table_file(report_dir + 'resultnewvars2_adjusted.tex', 
                            resultnewvars2_adjusted.summary())

modeltest2 = pd.Series(np.array([resultnewvars2_adjusted.fvalue, 
                                resultnewvars2_adjusted.f_pvalue]), 
                      index = ['F-test', 'P-value']).astype(float)
modeltest2.name = 'ADJUSTED Model F Test'
print(modeltest2)

data_frame_to_latex_table_file(report_dir + 'model_test_adjusted.tex',
                               modeltest2)

print('\nWald-Test Completed. Summary Exported.\n')


print('\nQuestion 15.\n')

print('\nPlotting Residuals vs. Regressors.\n')

fig_num = 1
fig = plt.figure(num=fig_num)
ax = fig.add_subplot(111)
ax.scatter(datadropna['w/a'], resultnewvars2_adjusted.resid)
ax.grid(True, linestyle=':')
ax.set_xlabel(r'Race')
ax.set_ylabel(r'$e$')
ax.set_title(r'Residuals vs Race')
plt.savefig(figure_dir + 'Residuals_vs_race.png')
plt.show()
fig_num += 1

fig = plt.figure(num=fig_num)
ax = fig.add_subplot(111)
ax.scatter(datadropna['gender'], resultnewvars2_adjusted.resid)
ax.grid(True, linestyle=':')
ax.set_xlabel(r'Gender')
ax.set_ylabel(r'$e$')
ax.set_title(r'Residuals vs Gender')
plt.savefig(figure_dir + 'Residuals_vs_gender.png')
plt.show()
fig_num += 1


fig = plt.figure(num=fig_num)
ax = fig.add_subplot(111)
ax.scatter(datadropna['gkfreelunch'], resultnewvars2_adjusted.resid)
ax.grid(True, linestyle=':')
ax.set_xlabel(r'Free Lunch Eligibility')
ax.set_ylabel(r'$e$')
ax.set_title(r'Residuals vs Free Lunch Eligibility')
plt.savefig(figure_dir + 'Residuals_vs_Freelunch.png')
plt.show()
fig_num += 1

fig = plt.figure(num=fig_num)
ax = fig.add_subplot(111)
ax.scatter(datadropna['gkclasstype'], resultnewvars2_adjusted.resid)
ax.grid(True, linestyle=':')
ax.set_xlabel(r'Class Type')
ax.set_ylabel(r'$e$')
ax.set_title(r'Residuals vs Class Type')
plt.savefig(figure_dir + 'Residuals_vs_classtype.png')
plt.show()
fig_num += 1


fig = plt.figure(num=fig_num)
ax = fig.add_subplot(111)
ax.scatter(datadropna['gktyears'], resultnewvars2_adjusted.resid)
ax.grid(True, linestyle=':')
ax.set_xlabel(r'Years Teaching Experience')
ax.set_ylabel(r'$e$')
ax.set_title(r'Residuals vs Teaching Experience')
plt.savefig(figure_dir + 'Residuals_vs_yearsteaching.png')
plt.show()
fig_num += 1


fig = plt.figure(num=fig_num)
ax = fig.add_subplot(111)
ax.scatter(datadropna['gkabsent'], resultnewvars2_adjusted.resid)
ax.grid(True, linestyle=':')
ax.set_xlabel(r'Days Absent from School')
ax.set_ylabel(r'$e$')
ax.set_title(r'Residuals vs Days Absent')
plt.savefig(figure_dir + 'Residuals_vs_daysabsent.png')
plt.show()
fig_num += 1

fig = plt.figure(num=fig_num)
ax = fig.add_subplot(111)
ax.scatter(datadropna['gkclasssize'], resultnewvars2_adjusted.resid)
ax.grid(True, linestyle=':')
ax.set_xlabel(r'Class Size')
ax.set_ylabel(r'$e$')
ax.set_title(r'Residuals vs Class Size')
plt.savefig(figure_dir + 'Residuals_vs_classsize.png')
plt.show()
fig_num += 1

print('\nPlots Exported.\n')


print('\nQuestion 16.\n')
print('\nSee the report.\n')


print('\nQuestion 17.\n')
print('\nDone.\n')
print('\nAssignment Completed!\n')